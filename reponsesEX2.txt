Exercice 2 ( reponses aux questions )

Question 1

Plusieurs lecteurs peuvent accéder à la base en meme temps. En effet il suffit
que plusieurs processus appellent lire() en meme temps puis attendent en meme
temps. Condition C1 valide.

Plusieurs écrivains peuvent accéder à la base en meme temps. Dans la fonction
ecrire() rien n'empêche d'autre écrivains d'écrire ( ou plutot attendre un
certain nombre de millisecondes ) en meme temps. Condition C2 non valide.

Pareillement, un lecteur et un écrivain peuvent accéder a la base en meme temps.
Dans la fonction lire(), rien ne vérifie la présence d'un écrivain en train
d'écrire ( ou attendre x millisecondes ) et rien ne vérifie dans la fonction
ecrire() qu'un lecteur est déja en train de lire ( ou attendre x ms )

Question 3

Notre solution ( BaseV1 ) ne résoud pas le probléme de famine, en effet il
suffit que plusieurs lecteurs lisent à la suite et ne repasse jamais le
sémaphore à l'écrivain. Nous pouvons utiliser un autre sémaphore pour faire une
liste d'attente à l'acquisition des séphamores déja utilisés. Par exemple, si il
y a déja 2 lecteurs en temps normal, d'autre futurs lecteurs peuvent dépasser
l'écrivain et aller lire sans faire la queue. En utilisant un sémaphore pour
l'acquisition des précédents séphamore, lorsqu'un écrivain entre dans la file,
il attendra que tous les lecteurs actuels finissent de lire. Par contre, les
futurs lecteurs devront attendre derriere l'écrivain et qu'il finisse d'écrire
pour pouvoir lire. Plusieurs lecteurs peuvent toujours lire en meme temps, il
suffit qu'il n'y ai pas d'écrivain devant eux dans la file d'attente ( C1 valide
), un seul écrivain peut écrire à la fois, puisqu'à l'écriture il saisit le
sémaphore permettant l'utilisation de la base, et tout autre futur écrivain doit
attendre que ce sémaphore soit relaché ( C2 valide ) et de meme pour tout autre
futur lecteurs ( C3 valide )
0. Commandes makefile
---------------------

> make

Pour compiler tous les fichiers sources

> make clean

Pour supprimer les fichiers sources

1. Exercice 1
-------------

> make EX1Q1

Reponds à la question 1 de l'exercice 1

> make EX1Q2

Reponds à la question 2 de l'exercice 1

> make EX1Q3

Reponds à la question 3 de l'exercice 1

2. Exercice 2
-------------

> make EX2Q2

Reponds à la question 2 de l'exercice 2

> make EX2Q3

Reponds à la question 3 de l'exercice 2

Note: Quelques tests sont dans les fichiers testEX2Q2.txt et testEX2Q3.txt

Note: Les éléments de réponses sont dans reponsesEX2.txt 

3. Exercice 3
-------------

> make EX3

Reponds à l'exercice 3

> make EX3bis

Reponds à l'exercice 3 en prenant en compte le probleme de famine

4. Exercice 4
-------------

> make EX4TEST

Test de l'exercice 4 en monothread

> make EX4TESTTHREAD

Test de l'exercice 4 avec 1 thread par valeur 

> make EX4TESTTHREADLINE

Test de l'exercice 4 avec 1 thread par ligne

> make EX4Q1

Reponds à la question 1 de l'exercice 4.

> make EX4Q2

Reponds à la question 2 de l'exercice 4.

> make EX4Q1

Reponds à la question 3 de l'exercice 4.

Note: la taille des grandes matrices sont à 1000. Vous pouvez la modifier dans le fichier source (ex4/MainMatrix.java:27) mais avec la méthode
de 1 thread par valeur cela risque de prendre beaucoup de temps d'exécution, mais on peut voir plus clairement la gain de temps
en exécution lorsqu'on compare 1 thread par line et monothread

Note: il se peut aussi que les fichiers se recompile ( une erreur dans le makefile surement... ) quand on fait les précédentes commandes, de ce fait
la commande time de unix sera faussée

Note: les comparaisons de temps se trouvent dans le fichier reponsesEX4.txt

5. Exercice 5
-------------

Note: pour les questions de 1 et 2, au lieu de tourner indéfiniment ou faire interrupt() apres un certain temps, on utilise la politique de terminaison 
implementée dans l'exercice 3 avec un nombre tres grand pour en voir le déroulement.

> make EX5Q1

Reponds à la question 1 de l'exercice 5

> make EX5Q2

Reponds à la question 2 de l'exercice 5

> make EX5Q3

Reponds à la question 3 de l'exercice 5

Note: pour la question 3, on fait la supposition que ce qu'on doit voir sur la sortie standard est le message de fin produit par un producteur et rien d'autre.

> make EX5Q4

Reponds à la question 4 de l'exercice 5

Note: Les éléments de réponses se trouvent dans reponsesEX5.txt
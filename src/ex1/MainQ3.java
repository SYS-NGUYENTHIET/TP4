
package ex1;

// la classe permettant de répondre a la question 2
public class MainQ3 {
    public static void main(String[] args) {
        System.out.println("début");
        Fille a = new Fille();
        a.start();
        try {
            Thread.sleep(2000);
            a.interrupt();
            System.out.println("fin");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

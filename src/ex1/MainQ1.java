package ex1;

// la classe permettant de répondre a la question 1
public class MainQ1 {
    public static void main(String[] args) {
        System.out.println("début");
        Thread a = new Fille();
        a.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("fin");
    }
}


package ex1;

// la classe permettant de répondre a la question 2
public class MainQ2 {
    public static void main(String[] args) {
        System.out.println("début");
        Thread a = new Fille();
        a.start();
        try {
            a.join(); // cette instruction permet d'attendre que la thread fille termine son execution
            Thread.sleep(2000);
            System.out.println("fin");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

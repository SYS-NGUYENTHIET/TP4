package ex4;

import java.util.*;

public class OneThreadPerValue extends ComputeMethod {

    public Matrix compute(Matrix m1, Matrix m2) {
        int length = m1.get_m();
        int res_n = m1.get_n();
        int res_m = m2.get_m();
        int[] row;
        int[] col;

        // on store les colonne en mémpire, explication dans MonoThreadMethod.java

        int[][] listOfCol = new int[res_m][m2.get_n()]; 
        for (int x = 0; x < res_m; x++) {
            listOfCol[x] = m2.get_colonne(x);
        }

        Matrix res = new Matrix(res_n, res_m, this);
        List<Thread> threads = new LinkedList<Thread>();

        try {

            // pour chaque valeur on crée un thread qui va la calculer
            for (int i = 0; i < res_n; i++) {
                row = m1.get_ligne(i);
                for (int j = 0; j < res_m; j++) {
                    col = listOfCol[j];
                    // transfert du contexte
                    ThreadMatrixV1 thread = new ThreadMatrixV1(res, col, row, i, j, length);
                    threads.add(thread);
                    thread.start();
                }
            }

            // on attend que les threads finissent avant de retourner le résultat
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;

    }
}
package ex4;

public class Matrix {
    private int n; // nb ligne
    private int m; // nb colonne
    private int[][] data; // la donnée
    private ComputeMethod cm; // méthode de calcul ( monothread etc... )

    public Matrix(int n, int m, ComputeMethod cm) {
        this.n = n;
        this.m = m;
        this.data = new int[n][m];
        this.cm = cm;
    }

    public int get_n() {
        return this.n;
    }

    public int get_m() {
        return this.m;
    }

    // i : ligne, j : colonne
    public int get(int i, int j) {
        return this.data[i][j];
    }

    public int[] get_ligne(int i) {
        return this.data[i];
    }

    public int[] get_colonne(int j) {
        int[] res = new int[this.n];

        for (int i = 0; i < this.n; i++) {
            res[i] = this.data[i][j];
        }
        return res;
    }

    public void set(int i, int j, int value) {
        this.data[i][j] = value;
    }

    public void print() {
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.m; j++) {

                System.out.print(this.get(i, j) + "\t");
                if (j == (this.m - 1)) {
                    System.out.print("\n");
                }

            }
        }
    }

    // condition : row and col same length
    public static int computeRowCol(int[] row, int[] col, int n) {
        if (row.length != col.length) {
            throw new IllegalArgumentException("row and col not same length");
        } else {
            int res = 0;
            for (int i = 0; i < n; i++) {
                res += (row[i] * col[i]);
            }
            return res;
        }
    }

    public Matrix product(Matrix matrix) {
        return cm.compute(this, matrix);
    }
}
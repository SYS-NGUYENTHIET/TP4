package ex4;

import java.util.*;

public class OneThreadPerLine extends ComputeMethod {

    public Matrix compute(Matrix m1, Matrix m2) {
        int length = m1.get_m();
        int res_n = m1.get_n();
        int res_m = m2.get_m();
        int[] row;

        // on store les colonnes de la 2e matrice en mémoire, explication dans
        // MonoThreadMethod.java
        int[][] listOfCol = new int[res_m][m2.get_n()];
        for (int x = 0; x < res_m; x++) {
            listOfCol[x] = m2.get_colonne(x);
        }

        Matrix res = new Matrix(res_n, res_m, this);
        List<Thread> threads = new LinkedList<Thread>();

        try {

            // pour chaque ligne, on va créer un thread qui va calculer toutes les valeurs
            // de cette ligne
            for (int i = 0; i < res_n; i++) {
                row = m1.get_ligne(i);

                // transfert du contexte
                ThreadMatrixV2 thread = new ThreadMatrixV2(res, listOfCol, row, i, length);
                threads.add(thread);
                thread.start();
            }

            // on attend que les threads finissent pour envoyer le resultat de la nouvelle matrice
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;

    }
}
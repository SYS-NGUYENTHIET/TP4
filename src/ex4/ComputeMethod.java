package ex4;

// cette classe définie la méthode de calcul ( c'est a dire mono thread, 1 thread par valeur, 1 thread par ligne)
public abstract class ComputeMethod {
    public abstract Matrix compute(Matrix m1, Matrix m2);
}
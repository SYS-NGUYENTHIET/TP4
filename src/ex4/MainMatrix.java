package ex4;

import java.util.Random;

public class MainMatrix {
    public static void main(String[] args) {
        if (args.length == 2) {

            boolean smallScaleTest;
            int n; // nombre de ligne de la premiere matrice
            int m; // nombre de colonne/ligne de la premiere matrice/seconde matrice ( respectivement )
            int m2; // nombre de colonne de la seconde matrice
            int size; // taille prédéfinie pour les tests a grande échelle
            int max; // valeur maximal pour chaque cellule

            Random random = new Random();
            if (args[0].equals("SMALL_SCALE_TEST")) {
                // test à petite echelle, utile pour vérifier le calcul
                size = 4;
                max = 5;
                n = random.nextInt(size) + 1;
                m = random.nextInt(size) + 1;
                m2 = random.nextInt(size) + 1;
                smallScaleTest = true;
            } else {
                // test à grande echelle, utile pour calculer le temps de calcul
                size = 1000;
                // size = 5000; attention, avec la méthode de 1 thread par valeur, le calcul risque de prendre beaucoup de temps...
                max = 100;
                n = size;
                m = size;
                m2 = size;
                smallScaleTest = false;
                if (!args[0].equals("LARGE_SCALE_TEST")) {
                    System.out.println("Warning: Invalid first argument, default settings to large scale test");
                }
            }

            ComputeMethod cm;
            if (args[1].equals("MONO_THREAD")) {
                cm = new MonoThreadMethod();
            } else {
                if (args[1].equals("ONE_THREAD_PER_LINE")) {
                    cm = new OneThreadPerLine();
                } else {
                    cm = new OneThreadPerValue();
                    if (!args[1].equals("ONE_THREAD_PER_VALUE")) {
                        System.out
                                .println("Warning: Invalid second argument, default settings to one thread per value");
                    }

                }
            }

            // remplissage de valeurs
            Matrix matrix = new Matrix(n, m, cm);
            
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    matrix.set(i, j, random.nextInt(max));
                }
            }

            Matrix matrix2 = new Matrix(m, m2, cm);

            for (int i = 0; i < m; i++) {
                for (int j = 0; j < m2; j++) {
                    matrix2.set(i, j, random.nextInt(max));
                }
            }

            Matrix matrix3 = matrix.product(matrix2);

            if (smallScaleTest) {
                // le test a petite échelle permet d'imprimer les matrices
                matrix.print();
                System.out.println("------ A");

                int lin = random.nextInt(n);
                int col = random.nextInt(m);

                // test les fonctions de get ligne et get colonne
                int[] res1 = matrix.get_ligne(lin);
                int[] res2 = matrix.get_colonne(col);

                System.out.println("Ligne " + lin + " : ");
                for (int i = 0; i < m; i++) {
                    System.out.print(res1[i] + " ");
                }
                System.out.print("\n");
                System.out.println("Colonne " + col + " : ");
                for (int j = 0; j < n; j++) {
                    System.out.print(res2[j] + " ");
                }
                System.out.print("\n");
                System.out.println("------");

                matrix2.print();
                System.out.println("------ B");

                // imprime le produit
                matrix3.print();
                System.out.println("------ A x B");
            }
        } else {
            System.out.println(
                    "Need 2 argument : LARGE_SCALE_TEST/SMALL_SCALE_TEST MONO_THREAD/ONE_THREAD_PER_VALUE/ONE_THREAD_PER_LINE");
        }
    }
}
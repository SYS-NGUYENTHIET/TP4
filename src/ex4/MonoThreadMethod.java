package ex4;

public class MonoThreadMethod extends ComputeMethod {

    public Matrix compute(Matrix m1, Matrix m2) {
        int length = m1.get_m();
        int res_n = m1.get_n();
        int res_m = m2.get_m();
        int[] row;
        int[] col;

        // on stock les colonnes de la deuxieme matrice dans la mémoire car
        // pour les obtenir c'est en Theta(n)
        // et on en a besoin plusieurs fois
        int[][] listOfCol = new int[res_m][m2.get_n()];

        for (int x = 0; x < res_m; x++) {
            listOfCol[x] = m2.get_colonne(x);
        }

        // la matrice de retour
        Matrix res = new Matrix(res_n, res_m, this);

        // pour chaque ligne, on prend tous les colonne et on fait le calcul avec computeRowCol
        for (int i = 0; i < res_n; i++) {
            row = m1.get_ligne(i);
            for (int j = 0; j < res_m; j++) {

                col = listOfCol[j];
                res.set(i, j, Matrix.computeRowCol(row, col, length));
            }
        }
        return res;

    }
}
package ex4;

// un thread par ligne
public class ThreadMatrixV2 extends Thread {
    private Matrix matrix;
    private int[][] cols; // the array of column we want to compute 
    private int[] row; // the row we want to compute 
    private int toBeModifiedRow; // the row we want to modify 
    private int length; // the length of the column and row to compute 

    public ThreadMatrixV2(Matrix matrix, int[][] cols, int[] row, int toBeModifiedRow, int length) {
        this.matrix = matrix;
        this.cols = cols;
        this.row = row;
        this.toBeModifiedRow = toBeModifiedRow;
        this.length = length;
    }

    @Override
    public void run() {
        int max = matrix.get_m();
        int res;
        for ( int i = 0; i<max;i++) {
            res = Matrix.computeRowCol(row, cols[i], length);
            this.matrix.set(this.toBeModifiedRow, i, res);
        }
    }
}
package ex4;

// un thread par valeur
public class ThreadMatrixV1 extends Thread {
    private Matrix matrix;
    private int[] col; // the column we want to compute 
    private int[] row; // the row we want to compute 
    private int toBeModifiedCol; // the column we want to modify 
    private int toBeModifiedRow; // the row we want to modify 
    private int length; // the length of the column and row to compute 

    public ThreadMatrixV1(Matrix matrix, int[] col, int[] row, int toBeModifiedRow, int toBeModifiedCol, int length) {
        this.matrix = matrix;
        this.col = col;
        this.row = row;
        this.toBeModifiedCol = toBeModifiedCol;
        this.toBeModifiedRow = toBeModifiedRow;
        this.length = length;
    }

    @Override
    public void run() {
        int res = Matrix.computeRowCol(this.row, this.col, this.length);
        this.matrix.set(this.toBeModifiedRow, this.toBeModifiedCol, res);
    }
}
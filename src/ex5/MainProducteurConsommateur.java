package ex5;

import java.util.*;

public class MainProducteurConsommateur {
    public static void main(String[] args) {
        if (args.length == 4) {

            try {
                boolean sleep;
                boolean big;
                int max = 4;
                Thread[] threads = new Thread[max];

                if (args[1].equals("SLEEP")) {
                    sleep = true;
                } else {
                    sleep = false;
                    if (!args[1].equals("NO_SLEEP")) {
                        System.out.println("Warning: invalid 2nd argument, threads set to no sleep");
                    }
                }

                if (args[2].equals("BIG")) {
                    big = true;
                } else {
                    big = false;
                    if (!args[2].equals("SMALL")) {
                        System.out.println("Warning: invalid 3rd argument, BAL maximum product set to small");
                    }
                }

                BAL bal = new BAL(big);
                Thread perturbateur = new Perturbateur(bal, sleep);

                if (args[3].equals("PERTURBATOR")) {
                } else {
                    if (!args[3].equals("NO_PERTURBATOR")) {
                        System.out.println("Warning: invalid 4th argument, no perturbator disabled by default");
                    }
                }

                for (int i = 0; i < max; i += 2) {
                    threads[i] = (new Producteur(i, bal, sleep));
                    threads[i + 1] = (new Consommateur(i + 1, bal, sleep));
                }

                if (args[0].equals("Q1")) {
                    threads[0].start();
                    threads[1].start();
                } else {
                    for (int i = 0; i < max; i++) {
                        threads[i].start();
                    }
                    if (!args[0].equals("Q2")) {
                        System.out.println("Warning: invalid 1st argument, multiples threads by default");
                    }
                }

                if (args[3].equals("PERTURBATOR")) {
                    perturbateur.start();
                } else {
                    if (!args[3].equals("NO_PERTURBATOR")) {
                        System.out.println("Warning: invalid 4th argument, no perturbator disabled by default");
                    }
                }

                for (Thread t : threads) {
                    t.join();
                }

                if (args[3].equals("PERTURBATOR")) {
                    perturbateur.join();
                }

            } catch (

            InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Need 4 argument : please use the make commands for expected output");
        }
    }
}
package ex5;

import java.util.Random;

public class Perturbateur extends Thread {
    private BAL bal;
    private final Random r = new Random();
    private boolean sleep;

    public Perturbateur(BAL bal, boolean sleep) {
        this.bal = bal;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        boolean running = true;
        try {
            while (running) {
                if (this.sleep) {
                    Thread.sleep(this.r.nextInt(500) + 1);
                }
                running = this.bal.perturb();
            }
        } catch (InterruptedException e) {
            ;
        }
    }
}

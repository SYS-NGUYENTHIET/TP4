// producer

package ex5;

import java.util.Random;

public class Producteur extends Thread {
    private int id;
    private BAL bal;
    private boolean sleep;
    private final Random r = new Random();

    public Producteur(int id, BAL bal, boolean sleep) {
        this.id = id;
        this.bal = bal;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        boolean running = true;
        try {
            while (running) {
                if (this.sleep) {
                    Thread.sleep(this.r.nextInt(500) + 1);
                }
                running = this.bal.put(this.id);
            }
        } catch (InterruptedException e) {
            ;
        }
    }
}
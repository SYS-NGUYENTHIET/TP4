// producer

package ex5;

import java.util.Random;

public class Consommateur extends Thread {
    private int id;
    private BAL bal;
    private final Random r = new Random();
    private boolean sleep;

    public Consommateur(int id, BAL bal, boolean sleep) {
        this.id = id;
        this.bal = bal;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        boolean running = true;
        try {
            while (running) {
                if (this.sleep) {
                    Thread.sleep(this.r.nextInt(500) + 1);
                }
                running = this.bal.take(this.id);
            }
        } catch (InterruptedException e) {
            ;
        }
    }
}
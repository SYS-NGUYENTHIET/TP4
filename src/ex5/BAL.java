package ex5;

import java.util.*;
import java.util.concurrent.Semaphore;

public class BAL {
    private List<String> messages = new LinkedList<String>();
    private int nb = 0; // number of messages inside
    private int prodnb = 0; // number of message produced since creation
    private final Random r = new Random();
    private final int maxprod; // number of maximum allowed produced messages ( not counting the last one which is message de fin)
    private boolean firstToAskIfTerminated = true; // self-explanatory
    private boolean[] status = new boolean[2]; // 2 booleans that describes the bal

    public BAL(boolean big) {
        if (big) {
            this.maxprod = r.nextInt(500) + 500;
        } else {
            this.maxprod = r.nextInt(5) + 1; 
        }
        System.out.println("Maximum produced message :" + this.maxprod);
    }

    public String generateRandomString() {
        String res = "";

        int max = this.r.nextInt(10) + 1;
        String alphabet = "abcde";

        for (int i = 0; i < max; i++) {
            res += alphabet.charAt(this.r.nextInt(alphabet.length()));
        }

        return res;

    }

    // returned boolean is whether or not rerunning is allowed, for example,
    // when the limit of produced messages is hit, this will return false causing all the
    // thread who are in this function to stop running. this applies for the rest of the synchronized methods
    public synchronized boolean put(int id) {
        try {
            // we call isTerminated while saying that we are a producer,
            // this allows us to be chosen to write the last message
            this.isTerminated(true);
            String s;

            // status[1] is whether or not we are chosen to write the last message
            if (status[1]) {
                if (status[0]) {
                    s = "message de fin";
                    System.out.println("Producteur\t" + id + " a mis\t" + s);
                    this.messages.add(s);

                    // notify everyone who is waiting to stop producing / reading
                    notifyAll();
                }
                return false;
            } else {
                s = this.generateRandomString();
                this.messages.add(s);
                System.out.println("Producteur\t" + id + " a mis\t" + s);
                this.nb++;
                this.prodnb++;
                notify();
                return true;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized boolean take(int id) throws InterruptedException {
        // special case when the maximum of messages written is 0:
        // a deadlock can happen because a producer could write the ending message
        // and calls the notify method, while a consumer wasnt waiting. once a consumer
        // calls this function, it will wait undefinitely
        if (this.maxprod != 0) {
            while (this.nb == 0) {
                wait();
            }

            this.isTerminated(false);
            if (!status[1]) {
                String s = this.messages.remove(0);
                this.nb--;
                System.out.println("Consommateur\t" + id + " a lu\t" + s);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public synchronized boolean perturb() throws InterruptedException {
        // special case, same as above
        if (this.maxprod != 0) {
            while (this.nb == 0) {
                wait();
            }
            this.isTerminated(false);
            if (!status[1]) {
                Collections.reverse(this.messages);
                System.out.println("Un perturbateur a perturbé");
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public synchronized void isTerminated(boolean isProducer) {
        // la premiere valeur correspond au fait si c'est le premier
        // processus qui demande et que la valeur retournée
        // est false. la deuxieme valeur est si la BAL a atteint sa
        // terminaison

        this.status[0] = false;
        this.status[1] = this.prodnb >= this.maxprod;
        if (this.status[1] && this.firstToAskIfTerminated && isProducer) {
            this.firstToAskIfTerminated = false;
            this.status[0] = true;
        }
    }
}

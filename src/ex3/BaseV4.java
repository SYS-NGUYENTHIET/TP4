package ex3;

// La baseV3 ne résoud pas le probléme de famine. Dans cette solution, nous proposons de donner la priorité aux ecrivains.
// On fait passer les écrivains un par un et quand il n'y en a plus, on fait passer tous les lecteurs d'un coup.

public class BaseV4 extends BaseV3 {
    private int nbWaitingWriter = 0;

    public BaseV4(int n, int max) {
        super(n, max);
    }

    public synchronized void prendreVerrouLecteur() throws InterruptedException {
        // begin read
        while (this.activeWriter || this.nbWaitingWriter > 0) {
            wait();
        }

        this.nbreaders++;
    }

    public synchronized void libererVerrouLecteur() throws InterruptedException {
        // end read
        this.nbreaders--;
        if (this.nbreaders == 0) {
            notifyAll();
        }
    }

    public synchronized void prendreVerrouEcrivain() throws InterruptedException {
        // begin write

        this.nbWaitingWriter++;
        while (this.activeWriter || this.nbreaders > 0) {
            wait();
        }
        this.activeWriter = true;
        this.nbWaitingWriter--;
    }

    public synchronized void libererVerrouEcrivain() throws InterruptedException {
        // end write
        this.activeWriter = false;
        notifyAll();
    }
}

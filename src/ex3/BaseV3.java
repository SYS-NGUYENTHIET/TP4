package ex3;

import ex2.Base;

public class BaseV3 extends Base {

    protected boolean activeWriter = false;

    public BaseV3(int n, int max) {
        super(n, max);
    }

    // partie gestion multithread
    public synchronized void prendreVerrouLecteur() throws InterruptedException {
        // begin read
        if (this.activeWriter) {
            wait();
        }
        this.nbreaders++;
        notify();
    }

    public synchronized void libererVerrouLecteur() throws InterruptedException {
        // end read
        this.nbreaders--;
        if (this.nbreaders == 0) {
            notify();
        }
    }

    public synchronized void prendreVerrouEcrivain() throws InterruptedException {
        // begin write
        while (this.activeWriter || this.nbreaders > 0) {
            wait();
        }
        this.activeWriter = true;
    }

    public synchronized void libererVerrouEcrivain() throws InterruptedException {
        // end write
        this.activeWriter = false;
        notify();
    }
}
package ex2;

import java.util.Random;

public class Processus extends Thread {
    private Base b; // un lien vers la base sur laquelle on travaille
    private int id; // un identifiant pour mieux voir ce qu'il se passe lors des tests
    public static final Random rand = new Random();

    // permet un accés à la base
    public Processus(Base b, int id) {
        this.b = b;
        this.id = id;
    }

    public String toString() {
        return "Processus " + this.id;
    }

    public void run() {
        try {
            while (true) {
                Thread.sleep(rand.nextInt(500));
                
                // nous allons avoir plus de lecteur que d'écrivain pour tester le probleme des famines
                int n = rand.nextInt(100);
                if (n > 20) {
                    lire();
                } else {
                    ecrire();
                }
            }
        } catch (InterruptedException e) {
            ;
        }
    }

    // trouve un indice dans lequel écrire
    public int findIndex() {
        return rand.nextInt(this.b.getSize());
    }

    public void lire() throws InterruptedException {
        System.out.println(this.toString() + " attend pour \tlire...");
        this.b.prendreVerrouLecteur();

        System.out.println(this.toString() + " est en train de \tlire...");
        int i = this.findIndex();
        this.b.read(i);
        System.out.println(this.toString() + " a fini de \t\tlire...");

        this.b.libererVerrouLecteur();
    }

    public void ecrire() throws InterruptedException {
        System.out.println(this.toString() + " attend pour \tecrire...");
        this.b.prendreVerrouEcrivain();

        System.out.println(this.toString() + " est en train de \tecrire...");
        int i = this.findIndex();
        int n = rand.nextInt(100);

        this.b.write(i, n);
        System.out.println(this.toString() + " a fini de \t\tecrire...");

        this.b.libererVerrouEcrivain();
    }
}
package ex2;

import java.util.concurrent.Semaphore;

public class BaseV2 extends Base {
    protected Semaphore file = new Semaphore(1); // file d'attente pour éviter famine

    public BaseV2(int n, int max) {
        super(n, max);
    }

    // partie gestion multithread
    public void prendreVerrouLecteur() throws InterruptedException {
        // si un ecrivain possede le sémaphore file, il faudra attendre que cet
        // écrivain finisse d'attendre pour le sémaphore s1
        this.file.acquire();

        this.s2.acquire();

        this.nbreaders++;
        if (this.nbreaders == 1) {
            this.s1.acquire();
        }

        this.s2.release();
        this.file.release();
    }

    public void libererVerrouLecteur() throws InterruptedException {
        this.s2.acquire();

        this.nbreaders--;
        if (this.nbreaders == 0) {
            this.s1.release();
        }

        this.s2.release();
    }

    public void prendreVerrouEcrivain() throws InterruptedException {
        this.file.acquire();
        this.s1.acquire();
        this.file.release();
    }

    public void libererVerrouEcrivain() throws InterruptedException {
        this.s1.release();
    }
}
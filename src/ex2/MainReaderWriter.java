
package ex2;

import java.util.*;
import ex3.*;

public class MainReaderWriter {
    public static void main(String args[]) {
        if (args.length == 1) {
            Base b = new BaseV1(100, 100);
            if (args[0].equals("EX2Q2")) {
                b = new BaseV1(100, 100);
            } else {
                if (args[0].equals("EX2Q3")) {
                    b = new BaseV2(100, 100);
                } else {
                    if (args[0].equals("EX3")) {
                        b = new BaseV3(100, 100);
                    } else {
                        if (args[0].equals("EX3bis")) {
                            b = new BaseV4(100, 100);
                        }
                    }
                }
            }

            int i;
            int max = 10;
            Processus[] p = new Processus[max];
            for (i = 0; i < max; i++) {
                p[i] = new Processus(b, i);
                p[i].start();
            }
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (i = 0; i < max; i++) {
                p[i].interrupt();
            }
        } else {
            System.out.println("Need 1 argument : Q2 or Q3");
        }
    }
}
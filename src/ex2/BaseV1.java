package ex2;

// premiere solution ( reponse a la question 2 exercice 2)
public class BaseV1 extends Base {
    public BaseV1(int n, int max) {
        super(n, max);
    }

    // partie gestion multithread
    public void prendreVerrouLecteur() throws InterruptedException {

        // l'acquisition du verrou est une section critique, on ne veut pas que
        // 2 lecteurs déroulent le code suivant en meme temps. si c'était le cas
        // par exemple, il se peut que this.nbreaders soit incrémenté 2 fois et
        // qu'il n'y aura pas d'appel à this.s1.acquire. cela peut provoquer le
        // fait que des lecteurs lisent en meme temps qu'un écrivain écrive
        this.s2.acquire();

        this.nbreaders++;

        // si le lecteur est le premier à entrer, on saisit le semaphore
        // permettant aux ecrivains d'écrire
        if (this.nbreaders == 1) {
            this.s1.acquire();
        }

        this.s2.release();
    }

    public void libererVerrouLecteur() throws InterruptedException {
        // meme raison qu'au dessus
        this.s2.acquire();

        this.nbreaders--;

        // si le lecteur est le dernier à sortir, on relache le semaphore pour
        // permettre aux ecrivains d'écrire
        if (this.nbreaders == 0) {
            this.s1.release();
        }

        this.s2.release();
    }

    public void prendreVerrouEcrivain() throws InterruptedException {
        this.s1.acquire();
    }

    public void libererVerrouEcrivain() throws InterruptedException {
        this.s1.release();
    }

}
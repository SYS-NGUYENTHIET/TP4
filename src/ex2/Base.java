package ex2;

import java.util.Random;
import java.util.concurrent.Semaphore;

// la base de donnée, classe abstraite car plusieurs versions vont en dérouler dans les prochaines questions / exercices 
public abstract class Base {
    private int[] db;
    private int size;

    public static final Random rand = new Random();

    protected int nbreaders = 0; // nombre de lecteur en train de lire
    protected Semaphore s1 = new Semaphore(1); // semaphore écrivain / lecteurs
    protected Semaphore s2 = new Semaphore(1); // semaphore entre lecteurs

    // partie gestion multithread
    abstract public void prendreVerrouLecteur() throws InterruptedException;

    abstract public void libererVerrouLecteur() throws InterruptedException;

    abstract public void prendreVerrouEcrivain() throws InterruptedException;

    abstract public void libererVerrouEcrivain() throws InterruptedException;

    // crée la classe de la base
    public Base(int n, int max) {
        // initialise la base de donnée
        this.db = new int[n];
        this.size = n;
        Random r = new Random();

        // on remplie la base de donnée aléatoire
        int i;
        for (i = 0; i < n; i++) {
            db[i] = r.nextInt(max);
        }
    }

    // affiche l'état de la base
    public void print() {
        int i;
        String output;

        for (i = 0; i < this.size; i++) {
            output = i + ": \t" + this.db[i] + "\n";
            System.out.print(output);
        }
    }

    public int getSize() {
        return this.size;
    }

    // retourne l'entier à l'indice i
    public int read(int i) throws InterruptedException {
        int res;

        // le travail de lecture prend un certain temps
        Thread.sleep(rand.nextInt(500));
        if (i > 0 && i < this.size) {
            res = this.db[i];
        } else {
            res = -1;
        }
        return res;
    }

    // écrit l'entier n à l'indice i
    public void write(int i, int n) throws InterruptedException {

        // le travail d'ecriture prend un certain temps
        Thread.sleep(rand.nextInt(500));

        if (i > 0 && i < this.size) {
            this.db[i] = n;
        }
    }
}
SRC_DIR = src
BLD_DIR = classes

.PHONY = clean


### Compilation
all: $(patsubst $(SRC_DIR)/%.java,$(BLD_DIR)/%.class,$(wildcard $(SRC_DIR)/**/*.java))

$(BLD_DIR)/%.class: $(BLD_DIR) $(SRC_DIR)/%.java
	javac -cp $(SRC_DIR) $(word 2,$^) -d $(word 1,$^)

$(BLD_DIR):
	mkdir -p $(BLD_DIR)

### Exécution ###
EX1Q1: $(BLD_DIR)/ex1/MainQ1.class
	java -cp $(BLD_DIR) ex1.MainQ1

EX1Q2: $(BLD_DIR)/ex1/MainQ2.class
	java -cp $(BLD_DIR) ex1.MainQ2

EX1Q3: $(BLD_DIR)/ex1/MainQ3.class
	java -cp $(BLD_DIR) ex1.MainQ3

EX2Q2: $(BLD_DIR)/ex2/MainReaderWriter.class
	java -cp $(BLD_DIR) ex2.MainReaderWriter $@

EX2Q3: $(BLD_DIR)/ex2/MainReaderWriter.class
	java -cp $(BLD_DIR) ex2.MainReaderWriter $@

EX3: $(BLD_DIR)/ex2/MainReaderWriter.class
	java -cp $(BLD_DIR) ex2.MainReaderWriter $@

EX3bis: $(BLD_DIR)/ex2/MainReaderWriter.class
	java -cp $(BLD_DIR) ex2.MainReaderWriter $@

EX4TEST: $(BLD_DIR)/ex4/MainMatrix.class
	java -cp $(BLD_DIR) ex4.MainMatrix SMALL_SCALE_TEST MONO_THREAD

EX4Q1: $(BLD_DIR)/ex4/MainMatrix.class
	java -cp $(BLD_DIR) ex4.MainMatrix LARGE_SCALE_TEST MONO_THREAD

EX4TESTTHREAD: $(BLD_DIR)/ex4/MainMatrix.class
	java -cp $(BLD_DIR) ex4.MainMatrix SMALL_SCALE_TEST ONE_THREAD_PER_VALUE

EX4Q2: $(BLD_DIR)/ex4/MainMatrix.class
	java -cp $(BLD_DIR) ex4.MainMatrix LARGE_SCALE_TEST ONE_THREAD_PER_VALUE

EX4TESTTHREADLINE: $(BLD_DIR)/ex4/MainMatrix.class
	java -cp $(BLD_DIR) ex4.MainMatrix SMALL_SCALE_TEST ONE_THREAD_PER_LINE

EX4Q3: $(BLD_DIR)/ex4/MainMatrix.class
	java -cp $(BLD_DIR) ex4.MainMatrix LARGE_SCALE_TEST ONE_THREAD_PER_LINE

EX5Q1: $(BLD_DIR)/ex5/MainProducteurConsommateur.class
	java -cp $(BLD_DIR) ex5.MainProducteurConsommateur Q1 SLEEP BIG NO_PERTURBATOR

EX5Q2: $(BLD_DIR)/ex5/MainProducteurConsommateur.class
	java -cp $(BLD_DIR) ex5.MainProducteurConsommateur Q2 SLEEP BIG NO_PERTURBATOR

EX5Q3: $(BLD_DIR)/ex5/MainProducteurConsommateur.class
	java -cp $(BLD_DIR) ex5.MainProducteurConsommateur Q2 SLEEP SMALL NO_PERTURBATOR

EX5Q4: $(BLD_DIR)/ex5/MainProducteurConsommateur.class
	java -cp $(BLD_DIR) ex5.MainProducteurConsommateur Q2 NO_SLEEP BIG PERTURBATOR


### Clean ###
clean:
	rm -rf $(BLD_DIR)